﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlogEngine.Api.Data;
using BlogEngine.Api.Data.Entities;
using Microsoft.AspNetCore.Identity;

namespace BlogEngine.Api.Data
{
    public static class DbInitializer
    {
        public static void Initialize(BlogEngineDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for category if existing then return
            if (context.Categories.Any())
            {
                return;
            }

            var categories = new Category[] {
                new Category{ Name=".Net Core" },
                new Category{ Name="EF Core" },
            };

            context.Categories.AddRange(categories);
            context.SaveChanges();


            var posts = new Post[] {
                new Post{ CategoryId=1,Title="Get started with .Net Core",FullDescription="Create new .Net Core app" },
                new Post{ CategoryId=2,Title="Get started with EF Core",FullDescription="Apply Ef core code first" },
            };

            context.Posts.AddRange(posts);
            context.SaveChanges();

            var roles = new Role[] {
                new Role{ Name="User" },
                new Role{ Name="Admin" },
            };

            context.Roles.AddRange(roles);
            context.SaveChanges();


            PasswordHasher<string> passwordHasher = new PasswordHasher<string>();
            var users = new User[] {
                new User{Username="user",Password=passwordHasher.HashPassword("user","123456"),Email="user@gmail.com",FirstName="John", LastName="Smith" },
                new User{Username="admin",Password=passwordHasher.HashPassword("admin","123456"),Email="admin@gmail.com",FirstName="Ryan", LastName="Nguyen" },
            };

            context.Users.AddRange(users);
            context.SaveChanges();

            var userRoles = new UserInRole[] {
                new UserInRole{RoleId=1,UserId=1 },
            };

            context.UserInRoles.AddRange(userRoles);
            context.SaveChanges();


        }
    }
}
