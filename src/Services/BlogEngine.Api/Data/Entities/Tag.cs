﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Api.Data.Entities
{
    public class Tag : BaseEntity
    {
        public Tag()
        {
            PostTags = new HashSet<PostTag>();
        }

        public string Title { get; set; }
        public virtual ICollection<PostTag> PostTags { get; set; }
    }
}
