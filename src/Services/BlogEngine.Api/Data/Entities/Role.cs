﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Api.Data.Entities
{
    public class Role : BaseEntity
    {
        public Role()
        {
            UserRoles = new HashSet<UserInRole>();
        }

        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<UserInRole> UserRoles { get; set; }

    }
}
