﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Api.Data.Entities
{
    public class BaseEntity
    {
        public virtual int Id { get; set; }
    }
}
