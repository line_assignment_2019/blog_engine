﻿using BlogEngine.Api.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogEngine.Api.Data.Configurations
{
    public class UserInRoleConfiguration : IEntityTypeConfiguration<UserInRole>
    {
        public void Configure(EntityTypeBuilder<UserInRole> builder)
        {
            builder.HasKey(o=>new {o.RoleId, o.UserId});

            builder
                .HasOne(o=>o.Role)
                .WithMany(o=>o.UserRoles)
                .HasForeignKey(o=>o.RoleId);
            builder
                .HasOne(o=>o.User)
                .WithMany(o=>o.UserRoles)
                .HasForeignKey(o=>o.UserId);
            
        }
    }
}
